# 阿里云 Zblog 框架组件

## 快速体验

- 初始化项目：`s init devsapp/start-zblog`
- 进入项目后部署：`s deploy`
- 部署过程中可能需要阿里云密钥的支持，部署完成之后会获得到临时域名可供测试

动画演示：

![]( ./terminal.gif)



## 快速导航

- [示例Yaml](#示例Yaml)
	- [Yaml字段说明](#Yaml字段说明)
- [额外说明](#额外说明)
	- [NAS+Container模式](#NASContainer模式)
	- [Container模式](#Container模式)
	- [模式对比](#模式对比)

## 示例Yaml

```yaml
edition: 1.0.0          #  命令行YAML规范版本，遵循语义化版本（Semantic Versioning）规范
name: zblogApp          #  项目名称
access: aliyun-release  #  秘钥别名

services:
  framework-test:                 #  服务名称
    component: zblog  # 组件名称
    props:
      region: cn-shenzhen         # 阿里云函数计算所支持的地区
      service:
        name: web-zblog   # 服务名
        description: Zblog framework service # 所在服务描述
        # logConfig: auto         # 日志配置，可以是auto
        # logConfig:
        #    logStore: test       # loghub中的logstore名称
        #    project: test        # loghub中的project名称
        # role: auto              # 角色
        # role: 
        #    name: test           # 角色名
        #    statement:           # 角色授权 statement，配置后不生效 service 不生效， statement 和 service 必填其一
        #        - Effect: Deny                 # Effect设置，同意或拒绝
        #          Action: sts:AssumeRole       # 行为，动作
        #          Principal: Principal
      function:
        name: zblog # 函数名
        description: Zblog framework function    # 函数描述
        # customContainerConfig:                # 自定义镜像配置
        #  image: registry.cn-shenzhen.aliyuncs.com/test-wss/nodejs12:v0.1  # 镜像仓库地址
        #  command: '["node", "index.js"]'      # 启动指令
        #  args: '["--port", "9000"]'           # 启动参数
        caPort: 9000          # 监听端口
        code:                 # 代码包配置
          src: ./src          # 本地路径
          excludes:           # ignore文件
            - package-lock.json
        # environmentVariables: # 环境变量配置
        #  test: demo
      trigger:                # 触发器配置
        name: httpTrigger     # 触发器名称
        type: http            # 触发器类型，只支持http
        config:               # 触发器详情
          authType: anonymous # 权限配置
          methods:            # 请求方法
            - GET
            - POST
            - PUT
      customDomains:          # 自定义域名配置
        - domainName: auto    # 自动获取域名，*.devsapp.cn
          protocol: HTTP      # 协议
          routeConfigs:       # 路径配置
            - path: '/*'   
```

### Yaml字段说明

- [字段解析](#字段解析)
    - [service字段](#service字段)
        - [role](#role)
        - [logConfig](#logConfig)
        - [vpcConfig](#vpcConfig)
        - [nasConfig](#nasConfig)
    - [function字段](#function字段)
        - [code](#code)
        - [customContainerConfig](#customContainerConfig)
        - [environmentVariables](#environmentVariables)
    - [triggers字段](#triggers字段)
        - [Http触发器](#Http触发器)
    - [customDomains字段](#customDomains字段)
        - [certConfig](#certConfig)
        - [routeConfigs](#routeConfigs)


#### 字段解析

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| region | True | Enum | 地域 |
| service | True | Struct | 服务 |
| function | True | Struct | 函数 |
| triggers | True | Struct | 触发器 |
| customDomains | True | Struct | 自定义域名 |

地区目前支持：`cn-beijing`, `cn-hangzhou`, `cn-shanghai`, `cn-qingdao`, `cn-zhangjiakou`, `cn-huhehaote`, `cn-shenzhen`, `cn-chengdu`, `cn-hongkong`, `ap-southeast-1`, `ap-southeast-2`, `ap-southeast-3`, `ap-southeast-5`, `ap-northeast-1`, `eu-central-1`, `eu-west-1`, `us-west-1`, `us-east-1`, `ap-south-1`

##### service字段
| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| name | True | String | service名称 |
| description | True | String | Service的简短描述 |
| internetAccess | False | Boolean | 设为true让function可以访问公网 |
| tracingConfig | False | String | 链路追踪，可取值：Enable、Disable |
| role | False | String[简单配置]/Struct[详细配置] | 授予函数计算所需权限的RAM role, 使用场景包含 1. 把 function产生的 log 发送到用户的 logstore 中 2. 为function 在执行中访问其它云资源生成 token |
| logConfig | False | Enum[简单配置]/Struct[详细配置] | log配置，function产生的log会写入这里配置的logstore |
| vpcConfig | False | Enum[简单配置]/Struct[详细配置] | VPC配置, 配置后function可以访问指定VPC |
| nasConfig | False | Enum[简单配置]/Struct[详细配置] | NAS配置, 配置后function可以访问指定NAS |

###### role

当`role`参数为字符串时，可以是：`acs:ram::xxx:role/qianfeng-fc-deploy-test-role`

当`role`参数为结构时，可以参考：

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| name | True | String | 角色名 |
| policies | True | List<String> | 策略列表 |

例如：

```
role:
  name: roleName
  policies:
    - AliyunOSSFullAccess
    - name: myPolicy
      description: custom policy
      statement: 
      - Effect: Allow
        Action: 
          - log:ListProject
        Resource:
          - acs:log:*:*:project/*
```

###### logConfig

当`logConfig`参数为简单配置是，可以是：`auto`

当`logConfig`参数为结构时，可以参考：

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| logStore | False | String | loghub中的logstore名称 |
| project | False | String | loghub中的project名称 |
| enableRequestMetrics | False | Boolean | RequestMetrics开关，取值`true`/`false` |
| enableInstanceMetrics | False | Boolean | InstanceMetrics开关，取值`true`/`false` |

###### vpcConfig

当`vpcConfig`参数为简单配置是，可以是：`auto`

当`vpcConfig`参数为结构时，可以参考：

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| securityGroupId | False | String | 安全组ID |
| vSwitchIds | False | List<String> | 一个或多个VSwitch ID |
| vpcId | False | String | VPC ID |

###### nasConfig

当`nasConfig`参数为简单配置是，可以是：`auto`

当`nasConfig`参数为结构时，可以参考：

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| mountPoints | False | List<Struct>[多目录配置] | 目录配置 |
| userId | False | String | userID, 默认为10003 |
| groupId | False | String | groupID, 默认为10003 |

####### mountPoints

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| serverAddr | False | String | NAS 服务器地址 |
| nasDir | False | String | NAS目录 |
| fcDir | False | String | 函数计算目录 |


##### function字段
| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| name | True | String | function名称 |
| description | False | String | function的简短描述 |
| timeout | False | Number | function运行的超时时间 |
| code | True | Object | 代码路径 |
| caPort | False | Number | CustomContainer/Runtime指定端口 |
| customContainerConfig | False | Struct | 自定义镜像配置 |
| environmentVariables | False | List<Struct> | 环境变量 |

###### code
| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| src | True | String | 代码路径 |
| excludes | False | List<String> | 除去某些文件 |

###### customContainerConfig
| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| image | False | String | 仓库地址 |
| command | False | String | 指令 |
| args | False | String | 参数 |

###### environmentVariables

Object格式，例如：

```
TempKey: tempValue
```

##### triggers字段

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| name | True | String | 触发器名称 |
| type | True | Enum | 触发器类型 |
| role | False | String | 使用一个 RAM 角色的 ARN 为函数指定执行角色，事件源会使用该角色触发函数执行，请确保该角色有调用函数的权限 |
| sourceArn | False | String | 资源Arn |
| config | True | Struct | 触发器配置 |

type目前支持：`http`

### Http触发器

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| authType | True | String | 鉴权类型，可选值：anonymous、function |
| methods | True | List | HTTP 触发器支持的访问方法，可选值：GET、POST、PUT、DELETE、HEAD |


## customDomains字段

| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| domainName | True | String | 域名，如果是auto取值，系统则会默认分配域名 |
| protocol | True | String | 协议，取值：`HTTP`, `HTTPS`, `HTTP, HTTPS` |
| routeConfigs | True | List<Struct> | 路由 |
| certConfig | False | Struct | 域名证书 |

> 相关权限可以参考[权限文档](./authority/yaml.md#自定义域名相关权限配置)

### certConfig
| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| certName | False | String | 名称 |
| privateKey | False | String | 表示私钥 |
| certificate | False | String | 表示证书 |

### routeConfigs
| 参数名 |  必填  |  类型  |  参数描述  |
| --- |  ---  |  ---  |  ---  |
| path | True | String | 路径 |
| serviceName | False | String | 服务名 |
| functionName | False | String | 函数名 |


## 额外说明

该框架部署会支持两种模式：`NAS+Container`与`Container`

### NAS+Container模式

该模式将会采用Container方式进行项目的部署，同时代码将会上传到NAS中，在该模式中可以认为函数计算是一个纯粹的运行环境，而所有的业务代码均被放到NAS中。

在配置Yaml的时候，推荐在`command `填写启动脚本的命令，以案例[start-zblog](https://github.com/devsapp/start-zblog) 为例，可以是：`'[/mnt/auto/zblog/scripts/start.sh"]'`

对应的启动脚本可以参考[start-zblog](https://github.com/devsapp/start-zblog/blob/master/src/code/scripts/start.sh)


此处需要额外注意：
- command的内容是：`'[/mnt/auto/zblog/scripts/start.sh"]'`，其结构为：`NAS挂载的函数计算本地目录（即fcDir目录）` + `函数名` + `启动脚本`，例如`fcDir`目录为`/mnt/hello`，此时函数名为`test`，启动脚本为默认的`start.sh`，此时启动路径为`/mnt/hello/test/start.sh`，此时command可以写成`'[/mnt/hello/test/start.sh"]'`
- 除了对command修改之外，还需要对`start.sh`文件进行修改，要注意该文件中的NAS相对路径，例如：
	- `cp /mnt/auto/zblog/zblog.conf /etc/nginx/sites-enabled/`实际上应该是`cp ` + `NAS挂载的函数计算本地目录（即fcDir目录）` + `函数名` + `配置文件` + `/etc/nginx/sites-enabled/`，例如`fcDir`目录为`/mnt/hello`，此时函数名为`test`，配置文件为`demo.conf`，此时应该是`cp /mnt/hello/test/test.conf /etc/nginx/sites-enabled/`

### Container模式

该模式将会采用Container方式进行项目的部署，代码将会被放到镜像中，在该模式下，默认不会使用NAS。

在配置Yaml的时候，推荐在`command `填写启动脚本的命令，以案例[start-zblog](https://github.com/devsapp/start-zblog) 为例，可以是：`'[./start.sh"]'`

### 模式对比

| 模式 |  优势  |  劣势  |  使用方法  |
| --- |  ---  |  ---  | ---  |
| NAS+Container | 在一定程度上可以实现代码的零改造，包括部分缓存到当前项目目录下的行为，或者上传文件持久化到当前目录下的行为，都是可以支持的； | 由于代码等存储到了NAS，所以函数计算所具备的灰度能力没办法直接使用； | 【默认模式】也可以通过`s cli fc-default set web-framework nas`指定  |
| Container | 代码和环境都会放在函数计算的容器中，相对来说可以更好的支持灰度发布； | 由于代码被放入了镜像中，所以代码中涉及到当前目录缓存、持久化数据等情况，可能没办法更好的支持（因为实例会被释放），当代码中有缓存当前目录或者持久化数据，文件到当前目录的时候，需要进行代码的改造； | 可以通过`s cli fc-default set web-framework container`指定  |
