"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@serverless-devs/core");
var base_1 = __importDefault(require("./base"));
var WEB_FRAMEWORK = 'devsapp/web-framework';
var RUNTIME = 'php7.2';
function handlerInputs(inputs, command) {
    return __awaiter(this, void 0, void 0, function () {
        var credential;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    inputs.props.runtime = RUNTIME;
                    return [4 /*yield*/, core_1.getCredential(inputs.project.access)];
                case 1:
                    credential = _a.sent();
                    core_1.reportComponent('zblog', {
                        command: command,
                        uid: credential.AccountID,
                    });
                    return [2 /*return*/];
            }
        });
    });
}
var Component = /** @class */ (function (_super) {
    __extends(Component, _super);
    function Component() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 部署应用
     * @param inputs
     */
    Component.prototype.deploy = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'deploy')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.deploy(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 删除应用
     * @param inputs
     */
    Component.prototype.remove = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'remove')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.remove(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 构建应用
     * @param inputs
     */
    Component.prototype.build = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'build')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.build(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 打印日志
     * @param inputs
     */
    Component.prototype.logs = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'logs')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.logs(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 指标数据查看
     * @param inputs
     */
    Component.prototype.metrics = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'metrics')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.metrics(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 指标部署函数信息
     * @param inputs
     */
    Component.prototype.info = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'info')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.info(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 查看Nas目录
     * @param inputs
     */
    Component.prototype.ls = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'ls')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.ls(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 移除Nas文件/目录
     * @param inputs
     */
    Component.prototype.rm = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'rm')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.rm(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    /**
     * 拷贝文件到Nas/更新操作
     * @param inputs
     */
    Component.prototype.cp = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var webFramework;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, handlerInputs(inputs, 'cp')];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, core_1.loadComponent(WEB_FRAMEWORK)];
                    case 2:
                        webFramework = _a.sent();
                        return [4 /*yield*/, webFramework.cp(inputs)];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    var _a;
    __decorate([
        core_1.HLogger('ZBLOG'),
        __metadata("design:type", typeof (_a = typeof core_1.ILogger !== "undefined" && core_1.ILogger) === "function" ? _a : Object)
    ], Component.prototype, "logger", void 0);
    return Component;
}(base_1.default));
exports.default = Component;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsOENBQXdHO0FBQ3hHLGdEQUFtQztBQUNuQyxJQUFNLGFBQWEsR0FBRyx1QkFBdUIsQ0FBQztBQUM5QyxJQUFNLE9BQU8sR0FBRyxRQUFRLENBQUM7QUFhekIsU0FBZSxhQUFhLENBQUMsTUFBZSxFQUFFLE9BQWU7Ozs7OztvQkFDM0QsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO29CQUNaLHFCQUFNLG9CQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBQTs7b0JBQXZELFVBQVUsR0FBRyxTQUEwQztvQkFFN0Qsc0JBQWUsQ0FBQyxPQUFPLEVBQUU7d0JBQ3ZCLE9BQU8sU0FBQTt3QkFDUCxHQUFHLEVBQUUsVUFBVSxDQUFDLFNBQVM7cUJBQzFCLENBQUMsQ0FBQzs7Ozs7Q0FDSjtBQUVEO0lBQXVDLDZCQUFhO0lBQXBEOztJQW9GQSxDQUFDO0lBbEZDOzs7T0FHRztJQUNVLDBCQUFNLEdBQW5CLFVBQW9CLE1BQWU7Ozs7OzRCQUNqQyxxQkFBTSxhQUFhLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxFQUFBOzt3QkFBckMsU0FBcUMsQ0FBQzt3QkFDakIscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBeEMsc0JBQU8sU0FBaUMsRUFBQzs7OztLQUMxQztJQUNEOzs7T0FHRztJQUNVLDBCQUFNLEdBQW5CLFVBQW9CLE1BQWU7Ozs7OzRCQUNqQyxxQkFBTSxhQUFhLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxFQUFBOzt3QkFBckMsU0FBcUMsQ0FBQzt3QkFDakIscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBeEMsc0JBQU8sU0FBaUMsRUFBQzs7OztLQUMxQztJQUNEOzs7T0FHRztJQUNVLHlCQUFLLEdBQWxCLFVBQW1CLE1BQWU7Ozs7OzRCQUNoQyxxQkFBTSxhQUFhLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxFQUFBOzt3QkFBcEMsU0FBb0MsQ0FBQzt3QkFDaEIscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBdkMsc0JBQU8sU0FBZ0MsRUFBQzs7OztLQUN6QztJQUVEOzs7T0FHRztJQUNVLHdCQUFJLEdBQWpCLFVBQWtCLE1BQWU7Ozs7OzRCQUMvQixxQkFBTSxhQUFhLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBbkMsU0FBbUMsQ0FBQzt3QkFDZixxQkFBTSxvQkFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFBOzt3QkFBakQsWUFBWSxHQUFHLFNBQWtDO3dCQUNoRCxxQkFBTSxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUF0QyxzQkFBTyxTQUErQixFQUFDOzs7O0tBQ3hDO0lBQ0Q7OztPQUdHO0lBQ1UsMkJBQU8sR0FBcEIsVUFBcUIsTUFBZTs7Ozs7NEJBQ2xDLHFCQUFNLGFBQWEsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLEVBQUE7O3dCQUF0QyxTQUFzQyxDQUFDO3dCQUNsQixxQkFBTSxvQkFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFBOzt3QkFBakQsWUFBWSxHQUFHLFNBQWtDO3dCQUNoRCxxQkFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUF6QyxzQkFBTyxTQUFrQyxFQUFDOzs7O0tBQzNDO0lBQ0Q7OztPQUdHO0lBQ0csd0JBQUksR0FBVixVQUFXLE1BQWU7Ozs7OzRCQUN4QixxQkFBTSxhQUFhLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBbkMsU0FBbUMsQ0FBQzt3QkFDZixxQkFBTSxvQkFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFBOzt3QkFBakQsWUFBWSxHQUFHLFNBQWtDO3dCQUNoRCxxQkFBTSxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUF0QyxzQkFBTyxTQUErQixFQUFDOzs7O0tBQ3hDO0lBQ0Q7OztPQUdHO0lBQ1Usc0JBQUUsR0FBZixVQUFnQixNQUFlOzs7Ozs0QkFDN0IscUJBQU0sYUFBYSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBQTs7d0JBQWpDLFNBQWlDLENBQUM7d0JBQ2IscUJBQU0sb0JBQWEsQ0FBQyxhQUFhLENBQUMsRUFBQTs7d0JBQWpELFlBQVksR0FBRyxTQUFrQzt3QkFDaEQscUJBQU0sWUFBWSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBQTs0QkFBcEMsc0JBQU8sU0FBNkIsRUFBQzs7OztLQUN0QztJQUNEOzs7T0FHRztJQUNVLHNCQUFFLEdBQWYsVUFBZ0IsTUFBZTs7Ozs7NEJBQzdCLHFCQUFNLGFBQWEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUE7O3dCQUFqQyxTQUFpQyxDQUFDO3dCQUNiLHFCQUFNLG9CQUFhLENBQUMsYUFBYSxDQUFDLEVBQUE7O3dCQUFqRCxZQUFZLEdBQUcsU0FBa0M7d0JBQ2hELHFCQUFNLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEVBQUE7NEJBQXBDLHNCQUFPLFNBQTZCLEVBQUM7Ozs7S0FDdEM7SUFDRDs7O09BR0c7SUFDVSxzQkFBRSxHQUFmLFVBQWdCLE1BQWU7Ozs7OzRCQUM3QixxQkFBTSxhQUFhLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFBOzt3QkFBakMsU0FBaUMsQ0FBQzt3QkFDYixxQkFBTSxvQkFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFBOzt3QkFBakQsWUFBWSxHQUFHLFNBQWtDO3dCQUNoRCxxQkFBTSxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzRCQUFwQyxzQkFBTyxTQUE2QixFQUFDOzs7O0tBQ3RDOztJQWxGaUI7UUFBakIsY0FBTyxDQUFDLE9BQU8sQ0FBQztzREFBUyxjQUFPLG9CQUFQLGNBQU87NkNBQUM7SUFtRnBDLGdCQUFDO0NBQUEsQUFwRkQsQ0FBdUMsY0FBYSxHQW9GbkQ7a0JBcEZvQixTQUFTIn0=