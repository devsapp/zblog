import { HLogger, ILogger, loadComponent, getCredential, reportComponent } from '@serverless-devs/core';
import BaseComponent from './base';
const WEB_FRAMEWORK = 'devsapp/web-framework';
const RUNTIME = 'php7.2';
interface IInputs {
  props: any;
  project: {
    component: string;
    access: string;
    projectName: string;
  };
  appName: string;
  args: string;
  path: any;
}

async function handlerInputs(inputs: IInputs, command: string) {
  inputs.props.runtime = RUNTIME;
  const credential = await getCredential(inputs.project.access);

  reportComponent('zblog', {
    command,
    uid: credential.AccountID,
  });
}

export default class Component extends BaseComponent {
  @HLogger('ZBLOG') logger: ILogger;
  /**
   * 部署应用
   * @param inputs
   */
  public async deploy(inputs: IInputs) {
    await handlerInputs(inputs, 'deploy');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.deploy(inputs);
  }
  /**
   * 删除应用
   * @param inputs
   */
  public async remove(inputs: IInputs) {
    await handlerInputs(inputs, 'remove');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.remove(inputs);
  }
  /**
   * 构建应用
   * @param inputs
   */
  public async build(inputs: IInputs) {
    await handlerInputs(inputs, 'build');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.build(inputs);
  }

  /**
   * 打印日志
   * @param inputs
   */
  public async logs(inputs: IInputs) {
    await handlerInputs(inputs, 'logs');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.logs(inputs);
  }
  /**
   * 指标数据查看
   * @param inputs
   */
  public async metrics(inputs: IInputs) {
    await handlerInputs(inputs, 'metrics');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.metrics(inputs);
  }
  /**
   * 指标部署函数信息
   * @param inputs
   */
  async info(inputs: IInputs) {
    await handlerInputs(inputs, 'info');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.info(inputs);
  }
  /**
   * 查看Nas目录
   * @param inputs
   */
  public async ls(inputs: IInputs) {
    await handlerInputs(inputs, 'ls');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.ls(inputs);
  }
  /**
   * 移除Nas文件/目录
   * @param inputs
   */
  public async rm(inputs: IInputs) {
    await handlerInputs(inputs, 'rm');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.rm(inputs);
  }
  /**
   * 拷贝文件到Nas/更新操作
   * @param inputs
   */
  public async cp(inputs: IInputs) {
    await handlerInputs(inputs, 'cp');
    const webFramework = await loadComponent(WEB_FRAMEWORK);
    return await webFramework.cp(inputs);
  }
}